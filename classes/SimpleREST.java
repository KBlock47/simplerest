package com.example.kevin.flashcards.classes;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kevin.flashcards.interfaces.VolleyCallback;
import org.json.JSONException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

;

/**
 * Created by Kevin on 2/13/2018.
 */

public class SimpleREST {

    private Context context;

    public SimpleREST(Context context){
        this.context = context;
    }

    public void GetJSON (String url, final VolleyCallback callback) throws IOException {

        StringRequest req = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Get REQUEST", response);
                try {
                    callback.onSuccessResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.d("Get ERROR", error.getMessage());
            }
        });

        if (this.context != null){
            Volley.newRequestQueue(this.context.getApplicationContext()).add(req);
        }else {
            Log.d("ERROR CONTEXT","Context was null??");
        }
    }

    public void PostJSON (String url, final HashMap<String, String> params, final VolleyCallback callback) throws IOException {

        StringRequest req = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Get REQUEST", response);
                try {
                    callback.onSuccessResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.d("Get ERROR", error.getMessage());
                try {
                    callback.onSuccessResponse("-1");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }){
            @Override
            protected Map<String,String> getParams() {
                return params;
            }
        };

        if (this.context != null){
            Volley.newRequestQueue(this.context.getApplicationContext()).add(req);
        }else {
            Log.d("ERROR CONTEXT","Context was null??");
        }
    }

}
