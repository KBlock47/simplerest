# SimpleREST

Implements a POST and GET request with Android's Volley HTTP Library with proper callbacks

This way you can pass in some cool things :muscle:

MainActivity includes a POST request example

## Requirements

```
Make sure that you have the dependency added for volley (com.android.volley:volley:1.0.0)
```
