package com.example.kevin.flashcards;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.kevin.flashcards.classes.SimpleREST;
import com.example.kevin.flashcards.interfaces.VolleyCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        SimpleREST api = new SimpleREST(this);

        HashMap<String, String> postData = new HashMap<>();

        postData.put("cooldata","wow!");
        postData.put("holyheck","wozers!!");

        try {
            api.PostJSON("https://httpbin.org/post", postData, new VolleyCallback() {
                @Override
                public void onSuccessResponse(String result) throws JSONException {
                    JSONObject json = new JSONObject(result);

                    Log.d("POST DATA",json.toString());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
